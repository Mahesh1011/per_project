const express= require('express');

const router = express.Router();

const mongoClient = require('mongodb').MongoClient;



const Fetch = router.get('/',(req,res)=>{
    mongoClient.connect('mongodb://localhost:27017/mahesh',(err,db)=>{
        if(err){
            res.status(500).send("Internal Server Error");
            return;
        }
        else{
            console.log("DB connected");
            db.collection('employee').find().toArray((err,data)=>{
                if(err) throw err;
                else{
                    if(data.length > 0){
                        res.status(200).json(data);
                    }else{
                        res.status(404).send("Users not found");
                    }
                }
            })
        }
    })
});

module.exports = Fetch;
