const express = require('express');

const router = express.Router();

const bcrypt =  require('bcrypt');

const mongoClient = require('mongodb').MongoClient;

module.exports = router.post('/insert',async(req,res)=>{

    const hashpassword = await bcrypt.hash(req.body.password,12);
    var user = {
        "empId": parseInt(req.body.empId),
        "empName": req.body.empName,
        "gender": req.body.gender,
        "age": parseInt(req.body.age),
        "city": req.body.city,
        "email": req.body.email,
        "password": hashpassword
    }
    mongoClient.connect('mongodb://localhost:27017/mahesh',(err,db)=>{
        if(err) throw err;
        db.collection('employee').insertOne(user,(err,result)=>{
            res.status(201).send("Created Successfully");
        })
    })
});
