const express = require('express');

const router = express.Router();

const mc = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');

let login = router.get('/',(req,res)=>{
    const {email,password} = req.body;
    mc.connect('mongodb://localhost:27017/mahesh',(err,db)=>{
        if(err){
            res.status(500).send("Internal Server Error");
            // return;
        }
        else{
            db.collection('employee').findOne({email},(err,user)=>{
                if(err){
                    res.status(500).send("Internal Server Error");
                    // return;
                }

                if(!user){
                    res.status(404).send("User not found");
                    // return;
                }
                bcrypt.compare(password,user.password,(err,result)=>{
                    if(err){
                        res.status(500).send("Internal Server Error");
                        // return;
                    }
                    if(result){
                        res.send("<h1>Login SuccessFul</h1>")
                    }else{
                        res.status(401).send("<h1>Invalid Password</h1>")
                    }
                })
                
            })
        }
    })
});

module.exports = login;
