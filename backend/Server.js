const express = require('express');

const app = express();

// to parse object in the body 

app.use(express.json())

app.use('/fetch', require('./Components/Fetch'))
app.use('/login', require('./Components/Login'))
app.use('/register', require('./Components/Register'))

app.listen(1900,()=>{
    console.log("Serever Running...");
})
